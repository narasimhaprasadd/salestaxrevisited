package com;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class TaxTest {
    @Test

    public void shouldBeAbleTotakeTaxDuty() {
        assertEquals("10.0 5.0", new Tax(10.0, 5.0).toString());
    }

    @Test
    public void shouldBeAbleToAddExemptedItemWhenListIsEmpty() {
        Tax tax = new Tax(10, 5);
        tax.addExemption("food");

        assertTrue(tax.isExempted("food"));

    }

    @Test
    public void shouldBeAbleToAddExemptedItemWhenListHasOneItem() throws Exception {
        Tax tax = new Tax(10, 5);
        tax.addExemption("food");
        tax.addExemption("medical");

        assertTrue(tax.isExempted("medical"));
    }


    @Test
    public void shouldBeAbleToCheckForItemNotExempted() throws Exception {
        Tax tax = new Tax(10, 5);
        tax.addExemption("food");
        assertFalse(tax.isExempted("medical"));
    }
}
