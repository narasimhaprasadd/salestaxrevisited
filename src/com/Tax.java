package com;

import java.util.ArrayList;
import java.util.List;

public class Tax {
    private double salesTax;
    private double importDuty;
    private List<String> exemptions=new ArrayList<String>();

    public Tax(double salesTax, double importDuty) {
        this.salesTax = salesTax;
        this.importDuty = importDuty;
    }

    public double getSalesTax() {
        return salesTax;
    }

    @Override
    public String toString() {
        return salesTax +
                " " + importDuty;
    }

    public boolean isExempted(String category) {
        return (exemptions.contains(category));
    }

    public double getImportDuty() {
        return importDuty;
    }


    public void addExemption(String item) {
        exemptions.add(item);
    }
}
