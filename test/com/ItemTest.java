package com;

import junit.framework.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ItemTest {
    @Test
    public void shouldBeAbleToTakeImportedItemDetails() {
        Item item=new Item("food",10.0,true,"food",1);
        Assert.assertEquals(item,new Item("food",10.0,true,"food",1));

    }

    @Test
    public void shouldBeAbleToTakeUnImportedItemDetails() {
        Item item=new Item("food",10.0,false,"food",1);
        Assert.assertEquals(item,new Item("food",10.0,false,"food",1));

    }
}
