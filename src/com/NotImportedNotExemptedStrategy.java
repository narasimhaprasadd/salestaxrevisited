package com;

public class NotImportedNotExemptedStrategy implements TaxCalcuationStrategy {
    @Override
    public double calculateTax(double price) {
        return 0;
    }
}
