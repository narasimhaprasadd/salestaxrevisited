package com;

public class ImportedNotExemptedStrategy implements TaxCalcuationStrategy {
    @Override
    public double calculateTax(double price) {
        return (float) (((price) * 10) / 100);
    }
}
