package com;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class ReceiptTest {

    @Test
    public void shouldBeAbleToCalculateTaxAndReturnCorrectTaxValueAndtotalValue(){
        ArrayList listOfItems=new ArrayList();
        listOfItems.add(new Item("book",12.49,false,"book",1));
        listOfItems.add(new Item("music",14.99,false,"music",1));
        listOfItems.add(new Item("chockolet",0.85,false,"food",1));
        List<String> listOfExemptedItems = new ArrayList<String>();

        listOfExemptedItems.add("book");
        listOfExemptedItems.add("food");
        Receipt receipt =new Receipt();
        receipt.calculate(listOfItems,listOfExemptedItems);
        //CalculatedTaxvalues calculatedTaxvalues= receipt.calculate(listOfItems);

        assertEquals("Sales Tax - 1.5\nTotal - 29.83",receipt.toString());
    }


    @Test
    public void shouldBeAbleToCalculateTaxAndReturnCorrectTaxValueAndtotalValueForAllImportedItems(){
        ArrayList listOfItems=new ArrayList();
        listOfItems.add(new Item("chocolates",10.00,true,"food",1));
        listOfItems.add(new Item("perfume",47.50,true,"perfume",1));

        List<String> listOfExemptedItems = new ArrayList<String>();

        listOfExemptedItems.add("book");
        listOfExemptedItems.add("food");
        Receipt receipt =new Receipt();
        receipt.calculate(listOfItems,listOfExemptedItems);

        assertEquals("Sales Tax - 7.65\nTotal - 65.15",receipt.toString());
    }


    @Test
    public void shouldBeAbleToCalculateTaxAndReturnCorrectTaxValueAndtotalValueForBothImportedAndLocalItems(){
        ArrayList listOfItems=new ArrayList();
        listOfItems.add(new Item("pills",9.75,false,"medicine",1));
        listOfItems.add(new Item("perfume",27.99,true,"perfume",1));
        listOfItems.add(new Item("perfume",18.99,false,"perfume",1));
        listOfItems.add(new Item("chockolet",11.25,true,"food",1));
        List<String> listOfExemptedItems = new ArrayList<String>();

        listOfExemptedItems.add("book");
        listOfExemptedItems.add("medicine");
        listOfExemptedItems.add("food");
        Receipt receipt =new Receipt();
        receipt.calculate(listOfItems,listOfExemptedItems);

        assertEquals("Sales Tax - 6.7\nTotal - 74.68",receipt.toString());
    }

}
