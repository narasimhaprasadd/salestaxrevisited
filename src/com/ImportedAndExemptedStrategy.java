package com;

public class ImportedAndExemptedStrategy implements TaxCalcuationStrategy {
    @Override
    public double calculateTax(double price) {
        return (double) (((price) * 5)/ 100);
    }
}
