package com;

import java.util.List;

import static java.lang.Math.ceil;
import static java.lang.Math.round;

public class Receipt {
    private double taxSum = 0;
    private double priceSum = 0;
    private double totalPrice=0;


    public Receipt calculate(List<Item> listOfItems, List<String> exemptedItems) {
        double totalTax = 0;
        for (Object i : listOfItems) {
            Item item = (Item) i;
            totalTax += item.calculateTax(exemptedItems);
            totalPrice += item.updatedPrice();
        }
        taxSum = ((ceil(totalTax * 20)) / 20);
        priceSum = round((totalPrice+taxSum) * 100.0) / 100.0;
        return this;
    }

    @Override
    public String toString() {
        return "Sales Tax - " + taxSum +
                "\nTotal - "+ priceSum;
    }
}
