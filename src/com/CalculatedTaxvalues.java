package com;

public class CalculatedTaxvalues {
    private double taxSum;
    private double priceSum;

    public CalculatedTaxvalues(double taxSum, double priceSum) {
        this.taxSum = taxSum;
        this.priceSum = priceSum;
    }

    @Override
    public String toString() {
        return "Sales Tax - " + taxSum +
                "\nTotal - " + priceSum;
    }
}
