package com;

public interface TaxCalcuationStrategy {
    double calculateTax( double price);
}
