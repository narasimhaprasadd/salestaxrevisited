package com;

public class ImportedAndNotExemptedStrategy implements TaxCalcuationStrategy {
    @Override
    public double calculateTax(double price) {
       return  (double) ((price * (15))/ 100);
    }
}
