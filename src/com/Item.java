package com;

import java.util.List;

import static java.lang.Math.ceil;
import static java.lang.Math.round;

public class Item {
    private String name;
    private double price;
    private boolean isImported;
    private String category;
    private double quantity;

    public Item(String name, double price, boolean isImported, String category, double quantity) {
        this.name = name;
        this.price = price;
        this.isImported = isImported;
        this.category = category;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return name + " " + price + " " + isImported + " " + category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (Double.compare(item.price, price) != 0) return false;
        if (isImported != item.isImported) return false;
        if (name != null ? !name.equals(item.name) : item.name != null) return false;

        return (this.name == item.name && this.price == item.price && this.isImported == item.isImported && this.category == item.category);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (isImported ? 1 : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }

    public double calculateTax(List<String> exemptedItems) {

        TaxCalcuationStrategy taxCalculationStrategy = getTaxCalculationStrategy(exemptedItems);

        return taxCalculationStrategy.calculateTax(price);

    }

    private TaxCalcuationStrategy getTaxCalculationStrategy(List<String> exemptedItems) {
        boolean isExempted = exemptedItems.contains(category);

        if (!isImported && isExempted) {
            return new NotImportedNotExemptedStrategy();
        } else if (!isImported && !isExempted) {
            return new ImportedNotExemptedStrategy();

        } else if (isImported && isExempted) {
            return new ImportedAndExemptedStrategy();

        }
        return new ImportedAndNotExemptedStrategy();

    }

    public double updatedPrice() {
        return quantity * price;
    }
}
